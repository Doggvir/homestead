# Homestead - The self hosted dashboard.

Homestead is an extensible dashboard application for your self hosted applications.

## Contributing

If you would like to contribute to the development of Homestead feel free to open a PR.

## License

Homestead is open-source software licensed under the [MIT license](http://opensource.org/licenses/MIT).